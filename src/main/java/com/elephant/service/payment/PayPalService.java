 package com.elephant.service.payment;

import com.elephant.config.PayPalConfig;

public interface PayPalService {
	
	public PayPalConfig getPayPalConfig();

}
