package com.elephant.model.cartitem;



import com.elephant.model.customer.CustomerModel;
//import com.elephant.model.cart.CartModel;
import com.elephant.model.uploadproduct.ProductModel;

public class CartItemModel {

	private long cartItemId;
	private ProductModel productModel;
	private int quantity;
	//private float totalprice;
	private CustomerModel customerModel;
	
	public CustomerModel getCustomerModel() {
		return customerModel;
	}
	public void setCustomerModel(CustomerModel customerModel) {
		this.customerModel = customerModel;
	}
	/*private CartModel cartModel;
	
	
	public CartModel getCartModel() {
		return cartModel;
	}
	public void setCartModel(CartModel cartModel) {
		this.cartModel = cartModel;
	}*/
	public long getCartItemId() {
		return cartItemId;
	}
	public void setCartItemId(long cartItemId) {
		this.cartItemId = cartItemId;
	}
	public ProductModel getProductModel() {
		return productModel;
	}
	public void setProductModel(ProductModel productModel) {
		this.productModel = productModel;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	/*public float getTotalprice() {
		return totalprice;
	}
	public void setTotalprice(float totalprice) {
		this.totalprice = totalprice;
	}
	*/
	
}
