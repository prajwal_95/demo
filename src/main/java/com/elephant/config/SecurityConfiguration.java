package com.elephant.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
@EnableWebSecurity
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
//	
//	@Override
//	protected void configure(HttpSecurity http ) throws Exception {
//		http.authorizeRequests().anyRequest().permitAll()
//		.and().httpBasic();
//		 http.csrf().disable();
//		
//	}
	
	@Autowired
	private DataSource dataSource;


	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().dataSource(dataSource)
	
		.usersByUsernameQuery("select email, password ,is_active=true, true from customers where email=?")
		.authoritiesByUsernameQuery("select user_email as principal, role_name as role from user_roles where user_email=?")
		
		.passwordEncoder(passwordEncoder()).rolePrefix("ROLE_");  
		
		
		
	}

	
	@Bean
	public PasswordEncoder passwordEncoder() {
		
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
//		 http  
//		    
//	        .authorizeRequests() 
//	        .antMatchers("/v2/api-docs", "/swagger-resources/configuration/ui", "/swagger-resources", "/swagger-resources/configuration/security", "/swagger-ui.html", "/webjars/**").permitAll()
//	        .and()
//	            .anyRequest().hasRole("ADMIN")  
//	            .and().formLogin().and()  
//	        .httpBasic()  
//	        .and()  
//	        .logout()  
//	        //.logoutUrl("/")  
//	        .logoutSuccessUrl("/")  
//	        ; 
		

		//http.cors().and().csrf().disable()
//		.authorizeRequests().antMatchers("/v1/add", "/v1/get/{Id}", "/v1/delete/{customerId}", "/v1/update","/v1/login2", "/css/**", "/webjars/**").permitAll()
//				.antMatchers("/profile").hasAnyRole("USER,ADMIN")
//				.antMatchers("/users1","/v1/add").hasRole("ADMIN")
//				.antMatchers("/v1/add","/addTask").permitAll()
//				
//				.and().formLogin().loginPage("/login2").permitAll()
//				.defaultSuccessUrl("/profile").and().logout().logoutSuccessUrl("/login");
		
		http.cors().and().csrf().disable().
        authorizeRequests()
        .antMatchers("/v1/**","/v2/**", "/swagger-resources/configuration/ui", "/swagger-resources", "/swagger-resources/configuration/security", "/swagger-ui.html", "/webjars/**").permitAll()
        .anyRequest().authenticated()
        .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}
	}




